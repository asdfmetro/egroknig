package com.example.jeanmi.egroking;

import java.util.Random;

/**
 * Created by jeanmi on 02/02/17.
 */

public class Character {
    private String name;
    // race: (0)human, (1)orc, (2)elf, (3)dwaf, (4)gnome
    // class : (0)none, (1)wizard, (2)warior, (3)thief,
    private int level, race, sex, classe, gold;
    //private ArrayList<cards> listStuff;

    Character(String name, int sex){
        this.name = name;
        this.level = 1;
        this.race = 0;
        this.classe = 0;
        this.sex = sex;
        this.gold = 0;
    }
    /* GETTERS */
    public int getClasse() {
        return classe;
    }
    public int getGold() {
        return gold;
    }
    public int getLevel() {
        return level;
    }
    public int getRace() {
        return race;
    }
    public int getSex() {
        return sex;
    }
    public String getName() {
        return name;
    }

    /* SETTERS */
    public void setSex(int sex) {
        this.sex = sex;
    }
    public void setClasse(int classe) {
        this.classe = classe;
    }
    public void setGold(int gold) {
        this.gold = gold;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public void setRace(int race) {
        this.race = race;
    }

    /* OTHER FUNCTION */

    public String getSexToString()
    {
        String stringSex = "";
        if(this.sex == 0)
        {
            stringSex="Guys";
        }
        else
        {
            stringSex="Poule";
        }
        return stringSex;
    }

    static public int getRandomNumber(int min, int max)
    {
        Random rvar = new Random();
        int randomgen = rvar.nextInt(max - min) + min;
        return randomgen;
    }
}
