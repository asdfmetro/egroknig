package com.example.jeanmi.egroking;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by jeanmi on 05/02/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MonstersDestroy";

    // players table name
    private static final String TABLE_CHAR_CHARACTER = "character";

    // players Table Columns names
    private static final String KEY_CHAR_ID = "id";
    private static final String KEY_CHAR_NAME = "name";
    private static final String KEY_CHAR_SEX = "sex";
    //private static final String KEY_ = "email";
    private final ArrayList<Character> character_list = new ArrayList<Character>();

    // players table name
    private static final String TABLE_CARDS = "cards";

    // players Table Columns names
    private static final String KEY_CARDS_ID = "id";
    private static final String KEY_CARDS_NAME = "name";
    private final ArrayList<Card> cards_list = new ArrayList<Card>();

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHARACTER_TABLE = "CREATE TABLE " + TABLE_CHAR_CHARACTER + "("
                + KEY_CHAR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_CHAR_NAME + " TEXT,"
                + KEY_CHAR_SEX + " INT " + ")";
        String CREATE_CARDS_TABLE = "CREATE TABLE " + TABLE_CARDS + "("
                + KEY_CARDS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_CARDS_NAME + " TEXT )";
        db.execSQL(CREATE_CHARACTER_TABLE);
        db.execSQL(CREATE_CARDS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAR_CHARACTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARDS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations for the Character table
     */

    // Adding new character
    public void Add_character(Character character) {
        int nextID = this.Get_Total_characters()+1;
        Log.v("INFOFOFOFOFO:::",nextID+" hiiihihihii");

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CHAR_ID, nextID);
        values.put(KEY_CHAR_NAME, character.getName()); // character Name
        values.put(KEY_CHAR_SEX, character.getSex()); // character Phone

        // Inserting Row

        db.insert(TABLE_CHAR_CHARACTER, null, values);
        db.close(); // Closing database connection
    }
    // Getting single player
    Character Get_character(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("Msg", "Coucou hibou coucou hibou "+id);
        //Character character=null;

        Cursor cursor = db.query(TABLE_CHAR_CHARACTER, new String[] {KEY_CHAR_ID,
                        KEY_CHAR_NAME, KEY_CHAR_SEX}, KEY_CHAR_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Character character = new Character(cursor.getString(1), Integer.parseInt(cursor.getString(2)));


        /*Cursor cursor = db.query(TABLE_CHAR_CHARACTER, new String[] { KEY_CHAR_ID,
                        KEY_CHAR_NAME, KEY_CHAR_SEX }, KEY_CHAR_ID + "=" + id,  null, null, null, null);

        if (cursor != null ) {
            cursor.moveToFirst();
            Log.d("Msg", "character id:"+cursor.getString(0)+" name:"+cursor.getString(1)+" sex:"+cursor.getString(2));
            character = new Character(cursor.getString(1), Integer.parseInt(cursor.getString(2)));


            // return contact

        }
        cursor.close(); */ //+" WHERE id = ?", new String[]{ ""+id }
        /*Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_CHAR_CHARACTER , null);
        if (cursor != null ) {
            cursor.moveToFirst();
            Log.d("Msg", "character id:"+cursor.getString(0)+" name:"+cursor.getString(1)+" sex:");
            character = new Character(cursor.getString(1), Integer.parseInt(cursor.getString(2)));


            // return contact

        }*/
        cursor.close();
        db.close();

        return character;
    }

    // Getting All players
    public ArrayList<Character> Get_characters() {
        try {
            character_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_CHAR_CHARACTER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Character character = new Character(cursor.getString(1), Integer.parseInt(cursor.getString(2)));
                    Log.d("Msg", "character id:"+cursor.getString(0)+" name:"+cursor.getString(1)+" sex:"+cursor.getString(2));
                    /*character.setID(Integer.parseInt(cursor.getString(0)));
                    character.setName(cursor.getString(1));
                    character.setPhoneNumber(cursor.getString(2));
                    character.setEmail(cursor.getString(3));*/
                    // Adding character to list
                    character_list.add(character);
                } while (cursor.moveToNext());
            }

            // return player list
            cursor.close();
            db.close();
            return character_list;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_player", "" + e);
        }

        return character_list;
    }

    // Updating single character
    public int Update_character(Character character) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CHAR_NAME, character.getName());
        values.put(KEY_CHAR_SEX, character.getSex());
        //values.put(KEY_EMAIL, character.getEmail());

        // updating row
        return db.update(TABLE_CHAR_CHARACTER, values, KEY_CHAR_ID + " = ?",
                new String[] { String.valueOf(character.getName()) });
    }

    // Deleting single player
    public void Delete_character(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CHAR_CHARACTER, KEY_CHAR_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.execSQL("UPDATE "+ TABLE_CHAR_CHARACTER +" set "+ KEY_CHAR_ID + "= (" + KEY_CHAR_ID + " - 1) WHERE " + KEY_CHAR_ID + " > "+ String.valueOf(id));
        db.close();
    }

    // Getting players Count
    public int Get_Total_characters() {
        String countQuery = "SELECT  * FROM " + TABLE_CHAR_CHARACTER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int nbcolumn = 0;
        if (cursor.moveToFirst()) {
            do {
                nbcolumn++;
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        // return count
        return nbcolumn;
    }


















    /**
     * All CRUD(Create, Read, Update, Delete) Operations for the Cards table
     */

    // Adding new cars
    public void Add_card(Card card) {
        int nextID = get_nb_cards()+1;
        Log.v("INFOFOFOFOFO:::",nextID+" hiiihihihii");

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CARDS_ID, nextID);
        values.put(KEY_CARDS_NAME, card.getName()); // card Name

        // Inserting Row

        db.insert(TABLE_CARDS, null, values);
        db.close(); // Closing database connection
    }
    // Getting single player
    Card Get_card(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("Msg", "Coucou hibou coucou hibou "+id);

        Cursor cursor = db.query(TABLE_CARDS, new String[] {KEY_CARDS_ID,
                        KEY_CARDS_NAME}, KEY_CARDS_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Card card = new Card( cursor.getString(1) );

        cursor.close();
        db.close();

        return card;
    }

    // Getting All cards
    public ArrayList<Card> Get_cards() {
        try {
            cards_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_CARDS;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Card card = new Card(cursor.getString(1));
                    cards_list.add(card);
                    Log.d("Msg", "Cards id:" + cursor.getString(0) + " name:" + cursor.getString(1));
                }while (cursor.moveToNext());
            }
            cursor.close();
            db.close();

        }
        catch (Exception e) {
            // TODO: handle exception
            Log.e("all_card", "" + e);
        }
        // return player list

        return cards_list;
    }

    // Deleting single card
    public void Delete_card(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CARDS, KEY_CARDS_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.execSQL("UPDATE "+TABLE_CARDS+" set "+ KEY_CARDS_ID + "= (" + KEY_CARDS_ID + " - 1) WHERE " + KEY_CARDS_ID + " > "+ String.valueOf(id));
        db.close();
    }

    // Getting players Count
    public int get_nb_cards()
    {
        String countQuery = "SELECT  * FROM " + TABLE_CARDS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int nbcolumn = 0;
        if (cursor.moveToFirst()) {
            do {
                nbcolumn++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return count
        return nbcolumn;
    }
}
