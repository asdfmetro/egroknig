package com.example.jeanmi.egroking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import static com.example.jeanmi.egroking.R.id.btnPlayerAdd;
import static com.example.jeanmi.egroking.R.id.btnPlayerCancel;


public class Frag_CreateCharacter extends Fragment implements View.OnClickListener  {
    private Button btnAdd;
    private Button btnCancel;
    private DatabaseHandler db;
    private EditText playerName;
    private RadioGroup radioGenderGroup;
    private View view;
    private RadioButton playerSex;
    private int genderplayer;



    public Frag_CreateCharacter() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_frag_create_character, container, false);
        btnAdd = (Button) view.findViewById(R.id.btnCreateChar);
        btnAdd.setOnClickListener(this);
        btnCancel = (Button) view.findViewById(R.id.btnPlayerCancel);
        btnCancel.setOnClickListener(this);

        genderplayer=0;

        playerName = (EditText) view.findViewById(R.id.EditTextPlayerName);
        radioGenderGroup=(RadioGroup)view.findViewById(R.id.playerSex);
        radioGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected

                switch(checkedId) {
                    case R.id.radioSexM:
                        genderplayer=0;
                        break;
                    case R.id.radioSexF:
                        genderplayer=1;
                        break;
                }
            }
        });




        return view;

    }
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnCreateChar:
                Log.v("INFO", "create");


                if(playerName.getText().toString().trim().equals(""))
                {
                    playerName.setError("name player required !");
                }
                else
                {


                    Character localplayer = new Character(playerName.getText().toString(),genderplayer);

                    db = new DatabaseHandler(this.getContext());
                    db.Add_character(localplayer);
                    db.close();

                }
                break;
            case R.id.btnPlayerCancel:
                Log.v("INFO", "cancel");
                break;
        }
        //getActivity().getFragmentManager().popBackStack("A", 0);
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
        Frag_ListCharacter fragment2 = new Frag_ListCharacter();
        fragmentTransaction2.addToBackStack("xyz");
        //fragmentTransaction2.hide(this);
        fragmentTransaction2.replace(R.id.frag_container, fragment2);
        fragmentTransaction2.commit();
        Log.v("INFO", "gofgidfogidfopgdfpogidfs");
    }

}
