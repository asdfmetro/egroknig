package com.example.jeanmi.egroking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class Frag_ListCharacter extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{
    private Button btnAdd;
    private DatabaseHandler db;
    private ArrayAdapter adapter;

    public Frag_ListCharacter() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_frag_list_character, container, false);
        btnAdd = (Button) view.findViewById(R.id.btnPlayerAdd);
        btnAdd.setOnClickListener(this);

        // print the character list
        db = new DatabaseHandler(this.getContext());
        ArrayList<Character> playerslist = db.Get_characters();

        String[] mobileArray = new String[playerslist.size()];
        int i = 0;
        for (Character currentplayer: playerslist) {
            mobileArray[i] = i + " : "+ currentplayer.getName();
            i++;
        }

        adapter = new ArrayAdapter<String>(this.getContext() ,R.layout.content_listview,mobileArray);

        ListView listView = (ListView) view.findViewById(R.id.listPlayer);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);


        return view;
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnPlayerAdd:
                Log.v("INFO", "add"); // i suck
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                Frag_CreateCharacter fragment2 = new Frag_CreateCharacter();
                fragmentTransaction2.addToBackStack("xyz");
                //fragmentTransaction2.hide(this);
                fragmentTransaction2.replace(R.id.frag_container, fragment2);
                fragmentTransaction2.commit();
                Log.v("INFO", "gofgidfogidfopgdfpogidfs");
                break;
        }


    }
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        // set parameter for the next fragment
        Bundle bundle = new Bundle();
        //bundle.putString("id_Character",  "You clicked Item: " + id + " at position:" + position );
        bundle.putInt("id_Character", position);
        // set next fragment
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
        Frag_ViewACharacter fragment2 = new Frag_ViewACharacter();
        fragment2.setArguments(bundle);

        fragmentTransaction2.replace(R.id.frag_container, fragment2);
        fragmentTransaction2.commit();
        Log.v("INFO", "gofgidfogidfopgdfpogidfs");
    }


}
