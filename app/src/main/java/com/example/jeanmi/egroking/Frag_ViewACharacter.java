package com.example.jeanmi.egroking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class Frag_ViewACharacter extends Fragment implements View.OnClickListener{

    private DatabaseHandler db;
    private Button btnCancel,deletePlayer,randomgeneration,btnStartGame;
    private Character currentCharacterToPrint;
    private TextView textViewRandom;
    private int idCharacter;

    public Frag_ViewACharacter() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_frag_view_a_character, container, false);
        idCharacter = this.getArguments().getInt("id_Character")+1;
        Log.v("INFOOOOOO:::", "character id to print : "+idCharacter);
        btnCancel = (Button) view.findViewById(R.id.btnCharCancel);
        btnCancel.setOnClickListener(this);

        db = new DatabaseHandler(this.getContext());
        currentCharacterToPrint = db.Get_character(idCharacter);

        TextView textViewnamePlayer = (TextView) view.findViewById(R.id.namePlayer);
        textViewnamePlayer.setText("name player : "+ currentCharacterToPrint.getName());

        TextView textViewSexPlayer = (TextView) view.findViewById(R.id.sexPlayer);
        textViewSexPlayer.setText("player sex : "+ currentCharacterToPrint.getSexToString());

        TextView textViewGoldPlayer = (TextView) view.findViewById(R.id.goldPalyer);
        textViewGoldPlayer.setText("player gold : "+String.valueOf(currentCharacterToPrint.getGold()));

        deletePlayer = (Button) view.findViewById(R.id.delete);
        deletePlayer.setOnClickListener(this);

        randomgeneration = (Button) view.findViewById(R.id.buttonGenRanom);
        randomgeneration.setOnClickListener(this);

        textViewRandom = (TextView) view.findViewById(R.id.randomNumber);

        btnStartGame = (Button) view.findViewById(R.id.btnplay);
        btnStartGame.setOnClickListener(this);

        return view;
    }
    @Override
    public void onClick(View view) {
        Log.v("INFO", "greturnofgidfogidfopgdfpogidfs");
        switch (view.getId()) {
            case R.id.btnCharCancel:
                // launch the other fragment
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                Frag_ListCharacter fragment2 = new Frag_ListCharacter();
                fragmentTransaction2.replace(R.id.frag_container, fragment2);
                fragmentTransaction2.commit();
                break;
            case R.id.delete:
                Log.d("Msg", "delete player " + idCharacter);
                db = new DatabaseHandler(this.getContext());
                db.Delete_character(idCharacter);
                db.close();
                // launch the other fragment
                FragmentManager fragmentManager3 = getFragmentManager();
                FragmentTransaction fragmentTransaction3 = fragmentManager3.beginTransaction();
                Frag_ListCharacter fragment3 = new Frag_ListCharacter();
                fragmentTransaction3.replace(R.id.frag_container, fragment3);
                fragmentTransaction3.commit();
                break;
            case R.id.buttonGenRanom:
                // random number generation
                textViewRandom.setText(String.valueOf(Character.getRandomNumber(1, 10)));
                break;
            case R.id.btnplay:
                textViewRandom.setText("the game will start");
                Intent intent = new Intent(getActivity(), StartGame.class);
                startActivity(intent);
                break;
        }
    }
}
