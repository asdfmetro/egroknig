package com.example.jeanmi.egroking;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnPlay:
                Intent intent = new Intent(getApplicationContext(), SelectCharacter.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }
}
