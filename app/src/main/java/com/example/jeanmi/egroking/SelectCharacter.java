package com.example.jeanmi.egroking;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SelectCharacter extends AppCompatActivity  {
    private Button btnPlayerAdd,btnPlayerCancel;
    private  Frag_CreateCharacter fragAddChar;
    private  Frag_ListCharacter fragListChar;
    //private FragmentTransaction myfratrans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_character);

      /*  btnPlayerAdd = (Button) findViewById(R.id.btnPlayerAdd);
        btnPlayerAdd.setOnClickListener(this);

        btnPlayerCancel = (Button) findViewById(R.id.btnPlayerCancel);
        btnPlayerCancel.setOnClickListener(this);
        btnPlayerCancel.setVisibility(View.GONE);*/

        fragAddChar = new Frag_CreateCharacter();
        fragListChar = new Frag_ListCharacter();

        //myfratrans = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, fragListChar).commit();

    }





}
