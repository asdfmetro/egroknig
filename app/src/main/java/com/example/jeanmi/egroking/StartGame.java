package com.example.jeanmi.egroking;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class StartGame extends AppCompatActivity {
    private StartGameFragment frag1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

        frag1 = new StartGameFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, frag1).commit();
    }

}
