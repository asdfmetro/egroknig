package com.example.jeanmi.egroking;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class StartGameFragment extends Fragment implements View.OnClickListener {
    private Button btnUpdate, btnReload;
    // URL to get the cards JSON
    private static String url = "http://ec2-35-157-27-135.eu-central-1.compute.amazonaws.com/data.json";
    //private ArrayList<String> cardsList;
    private String[] cardsList;
    private ListView cardsListView;
    private View view;
    private ProgressDialog pDialog;
    private ArrayAdapter adapter;
    private DatabaseHandler db;

    public StartGameFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_start_game, container, false);
        btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnReload= (Button) view.findViewById(R.id.btnreload);
        btnReload.setOnClickListener(this);

        //cardsList = new ArrayList<>();

        cardsListView = (ListView) view.findViewById(R.id.listremote);

        pDialog = new ProgressDialog(view.getContext());

        this.listReload();

        //new GetCards().execute();
        return view;
    }
    @Override
    public void onClick(View view) {
        Log.v("INFO", "greturnofgidfogidfopgdfpogidfs");
        switch(view.getId()) {
            case R.id.btnUpdate:
                // internet connection
                new GetCards().execute();
                break;
            case R.id.btnreload:
                // internet connection
                this.listReload();
                break;
        }
    }
    private void listReload(){
        db = new DatabaseHandler(this.getContext());
        ArrayList<Card> arraylistcards = db.Get_cards();

         cardsList = new String[arraylistcards.size()];
        int i = 0;
        for (Card currentcard: arraylistcards) {
            cardsList[i] = i + " : "+ currentcard.getName();
            Log.v("INFOOO","Test123 "+i);
            i++;
        }

        adapter = new ArrayAdapter<String>(this.getContext() ,R.layout.content_listview,cardsList);
        cardsListView.setAdapter(adapter);
    }
    /**
     * Async task class to get json by making HTTP call
     */
    private class GetCards extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e("INFO", "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray cards = jsonObj.getJSONArray("deck");
                    cardsList = new String[cards.length()];
                    // looping through All Contacts
                    for (int i = 0; i < cards.length(); i++) {
                        JSONObject c = cards.getJSONObject(i);

                        String id = c.getString("id");
                        String name = c.getString("name");

                        cardsList[i]= i + " : card name : " + name;
                        /*if(i==1)
                        {
                            db = new DatabaseHandler(view.getContext());
                            Card newcard = new Card(name);
                            db.Add_card(newcard);
                            db.close();
                        }*/
                    }
                } catch (final JSONException e) {
                    Log.e("INFO", "Json parsing error: " + e.getMessage());
                }
            } else {
                Log.e("INFO", "Couldn't get json from server.");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            adapter = new ArrayAdapter<String>(view.getContext() ,R.layout.content_listview,cardsList);

            cardsListView.setAdapter(adapter);
        }

    }
}
