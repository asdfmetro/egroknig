Welcome to my android project !
===================

This application is a draft/beginning of a role play card game.

### Table of contents

[TOC]


Main activities
==========


First activity
-------

When we start the application we arrive in this view.

![start activity](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-093209.png)

To go to the next activity you need to click on the central button.

Character activity 
-------
This activity is conceived with 3 fragments :

#### 1. List character
This fragment list all the characters from the local database.
 
![select character](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-093312.png)

 From here we can go to the new character[.2] (by clicking on the "+" top-right screen) and detail (by clicking on a character in the list) [3.] fragments.
#### 2. Create
 Fragment to a new character.

![create character](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-110634.png)

 Enter a name, select a gender for the character.
 Button "CREATE" create the new character.
 And the cross (top-right-screen) allows to cancel the creation and come back to the character's list fragment
 
#### 3. detail view
This view is used to see more detail on the character that you selected.

![detail view](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-093337.png)


From there we can as well delete the character (by clicking on the "DELETE" button, generate a random number between 1 and 9 (by clicking on the "RANDOM" button, and start the last activity (start the game) by clicking on the "PLAY" button.

Start game activity
-------
This activity will list the cards available for the game from the local database.

![local database](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-101657.png)

By clicking on the "GET REMOTE DATABASE" button you can print/update the local database with the remote one (right now it doesn't save in the local database for testing reason). 
This is the json that the application receive :

    {
    "deck": [
      { "id" : "1",
      "name": "card1" } ,
      { "id" : "2",
      "name": "card2" } ,
      { "id" : "3",
      "name": "card3" } ,
      { "id" : "4",
      "name": "card4" } 
    ]
    }

Here when this button is just clicked :

![remote DB](https://gitlab.com/asdfmetro/android-project-presentation/raw/master/device-2017-05-09-101731.png)

And by clicking on the "RELOAD LOCAL DATABASE" you will print the local database.


Functionnalities
======

 - 3 activities which use fragments
 - all the variable in private (if it is useful)
 - Local database : 2 Tables (CRUD)
 - Network : get json from a http REST API, to print and update the database (synchronisation with a remote database)


Links used
======

 - [HTTP/JSON PARSER](http://www.androidhive.info/2012/01/android-json-parsing-tutorial/)
 - [JSON creator](http://www.jsoneditoronline.org/)
 - [Database1](http://stackoverflow.com/questions/17529766/view-contents-of-database-file-in-android-studio) [database2](https://developer.android.com/training/basics/data-storage/databases.html)
 - [official android dev](developer.android.com)

